# No-BI #

No BI is a Caserta Concepts proof-of-concept to envision a BI tool based on search that gives the business user a powerful, intuitive view of their data


### How do I get set up? ###

* Install Solr and populate with data (we only used one data set during the proof-of-concept phase so there are some assumptions made in the code because of that)
* Clone the repo
* start solr (easy way is to cd to the directory with solr, cd into examples, and run "sudo java -jar start.jar"
* you can check if solr is running by browsing to localhost:8983/solr
* in another terminal, activate the virtual environment in the python project ("source bin/activate" from the root of the project)
* start django in live-reload development mode with "python manage.py runserver"