angular.module('nobiDirectives',['ui.bootstrap'])
.directive('nobiLogin', function(){

    var userInfoUrl = 'http://127.0.0.1:8000/noBIadmin/userinfo/?callback=JSON_CALLBACK';
    var loginUrl = 'http://127.0.0.1:8000/noBIadmin/login?callback=JSON_CALLBACK';

    var LoginCtrl = function ($scope, $modalInstance, $http, user) {
        $scope.user = user;

        $scope.doLogin = function (username, password) {
            // do login here

            $http({
                url: loginUrl,
                method: 'JSONP',
                params: {
                    username: username,
                    password: password
                }
            }).success(function(data) {
                    if (data.success) {
                        $scope.user.name = data.username;
                        $modalInstance.close($scope.user);
                    }
                    else {
                        $scope.user.isInvalid = true;
                        $scope.user.message = data.message;
                    }
                });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    return {
        template: '<a href=#" ng-click=login()>{{username}}</a>',
        restrict: 'E',
        link: function (scope, iElement, iAttrs) { },
        scope: {

        },
        replace:true,
        controller: function($scope, $http, $modal) {

            $http.jsonp(userInfoUrl).success(function(data) {
                $scope.username = (data == 'anonymous' ? 'Log In' : data);
            });

            $scope.login = function () {

                var modalInstance = $modal.open({
                    templateUrl: 'partials/login.html?v=1',
                    controller: LoginCtrl,
                    resolve: {
                        user: function () {
                            return { username: $scope.user };
                        }
                    }
                });

                modalInstance.result.then(function (user) {
                    $scope.username = user.name;
                });
            };
        }
    }
});
