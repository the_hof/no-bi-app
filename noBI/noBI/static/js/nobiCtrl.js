function nobiCtrl($scope, $routeParams, $http, $resource, $modal) {

    $scope.nobiURL = "http://localhost:8000/q/?callback=JSON_CALLBACK";
    $scope.nobiAutoCompleteURL = "http://localhost:8000/auto/?callback=JSON_CALLBACK";
    $scope.isPristine = true;
    clearInternalState();

    $scope.suggestions = function(query_text) {
        if ($scope.isPristine) {
            $scope.isPristine = false;
            document.getElementById("search_bar_top").focus();
        }

        var q = querystringEncode(query_text);
        var url = $scope.nobiAutoCompleteURL + '&q=' + q;

        return $http.jsonp(url).then(function(response){
            return response.data.autocomplete_results;
        });
    }

    $scope.hidePristineUI = function() {
        if ($scope.isPristine) {
            $scope.isPristine = false;
            document.getElementById("search_bar_top").focus();
        }
    }

    $scope.toggleFacet = function(facets) {
        f = querystringEncode(facets);

        if (f !== '') {
            var index = $scope.selectedFacets.indexOf(f);
            if (index != -1){
                $scope.selectedFacets.splice(index,1);
            } else {
                $scope.selectedFacets.push(f);
            }

            isQueryOnly = false;
        }
        else {
            $scope.selectedFacets = [];
        }
    }

    $scope.toggleFilter = function(filterKey, filterValue) {
        if (filterKey !== '') {
            var should_push = true;
            for(var i=0; i<$scope.filters.length; i++){
                if (($scope.filters[i][1] == filterValue) && ($scope.filters[i][0] == filterKey)) {
                    $scope.filters.splice(i,1);
                    should_push = false;
                }
            }

            if (should_push) $scope.filters.push([filterKey, filterValue]);
        }

        refreshData();
    }

    $scope.initializeSearch = function(query_text) {
        clearInternalState();

        $scope.isPristine = false;

        var q = querystringEncode(query_text);
        var url = $scope.nobiURL + '&q=' + q;
        $scope.query = query_text;
        $scope.queryText = query_text;
        $http.jsonp(url).success(processResultsCacheFacets);
    }

    // private methods
    function clearInternalState() {
        $scope.nobiCache = '';                                  // cache result here (may remove later)
        //$scope.query = '';                                      // query -> set on search
        $scope.queryText = '';
        $scope.selectedFacets = [];                             // ordered history of facets -> set on search
        $scope.filters = [];                                    // ordered history of filters applied -> set on search
        $scope.resultStatsFacets = [];
        $scope.resultStatsFacetsData = '';
        $scope.defaultDataViz = '';
        $scope.recommendedLinks = [];
    }

    function refreshData() {
        var q = querystringEncode($scope.query);
        var url = $scope.nobiURL + '&q=' + q;

        for (var i=0; i<$scope.filters.length; i++) {
            url += '&fq=' + querystringEncode($scope.filters[i][0]) +':\"'
            url += querystringEncode($scope.filters[i][1])+'\"';
        }

        $http.jsonp(url).success(processResults);
    }

    //when we don't care about the facets, land here
    function processResults(data) {
        $scope.nobiCache = data;

        var activity = data.stats.stats_fields.activity;

        $scope.resultStatsFacets = [];
        if (activity) {
            $scope.resultStatsFacetsData = activity.facets.dimension1;

            for (key in $scope.resultStatsFacetsData) {
                var tmp = $scope.resultStatsFacetsData[key];
                $scope.resultStatsFacets.push([key, formatMetric(tmp.sum)]);
            }
        } else {
            $scope.resultStatsFacetData='';
            $scope.resultStatsFacets.push(['No Data', formatMetric(0)]);
        }
    };

    // land here when we are loading facets as well as data
    function processResultsCacheFacets(data) {
        processResults(data);

        $scope.recommendedLinks = data.responseHeader.nobi_reports;

        $scope.defaultDataViz = data.responseHeader.nobi_dataviz;

        $scope.defaultFacets = data.responseHeader.nobi_facets;
        $scope.defaultFacetsArray = $scope.defaultFacets.split(',');

        if (data.facet_counts) {
            $scope.facetDrill = data.facet_counts.facet_fields;
        }
        else $scope.facetDrill=[];
    }

    //returns true if the facet is currently "selected" or "opened"
    $scope.facetSelected = function (facet_value) {
        return ($scope.selectedFacets.indexOf(facet_value) != -1);
    }

    //find the named facet data set
    $scope.getFacet = function(facet_value) {
        var val = eval('$scope.facetDrill.' + facet_value);
        return val;
    }

    //view logic
    //returns true if we should show a given facet
    $scope.showFacet = function(index, facet_array) {
        if ((index % 2) == 0) {
            if (facet_array[index+1] == 0) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    $scope.getFiltersByFacet = function(facet_name) {
        var facetList = [];

        for(var i=0; i<$scope.filters.length; i++) {
            if ($scope.filters[i][0] == facet_name) {
                facetList.push($scope.filters[i]);
            }
        }

        return facetList;
    }

    $scope.getSelectedFacetList = function () {
        var facetList = [];

        for (var i=0; i<$scope.filters.length; i++) {
            if (facetList.indexOf($scope.filters[i][0]) == -1) {
                facetList.push($scope.filters[i][0]);
            }
        }

        return facetList;
    }

    $scope.unselectFacet = function(f) {
        var tmp_filters = [];
        for (var i=0; i<$scope.filters.length; i++) {
            if ($scope.filters[i][0] != f) {
                tmp_filters.push($scope.filters[i]);
            }
        }

        $scope.filters = tmp_filters;
        refreshData();
    }

    function formatMetric(m) {
        return Math.abs(m);
    }

    function querystringEncode(s) {
        return String(s).replace(/\s/g, '+').replace("&", "%26");
    }
}
