function UsersCtrl($scope, $http) {

    var USER_ADD_URL = 'http://127.0.0.1:8000/noBIadmin/adduser?callback=JSON_CALLBACK';
    var USER_LIST_URL = 'http://127.0.0.1:8000/noBIadmin/users?callback=JSON_CALLBACK';
    $scope.username = '';

    var listUsers = function() {
        $http.jsonp(USER_LIST_URL).success(listUsersCallback);
    }

    var listUsersCallback = function(data) {
        $scope.users = data;
    }

    var createUserCallback = function(data) {
        $scope.result = data;
        $scope.result.class = ($scope.result.success ? 'success' : 'error');
        listUsers();
    }

    $scope.createUser = function() {
        $http.jsonp(USER_ADD_URL + '&username=' + $scope.username)
            .success(createUserCallback);
    }

    listUsers();
}