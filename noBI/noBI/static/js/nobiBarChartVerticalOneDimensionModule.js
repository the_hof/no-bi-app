d3.custom = {};

d3.custom.barChartVerticalOneFact = function module() {
  var margin = { top: 20, right: 20, bottom: 40, left: 80 },
    width = 600,
    height = 500,
    gap = 0,
    ease = 'cubic-in-out',
    defaultDataViz = '';
  prevDataViz = '';
  var svg, duration = 500;

  var currentSort = "";

  var dispatch = d3.dispatch('customHover');

  function exports(_selection) {

    //d3.select("svg").remove();
    vBar_chart1(_selection);

  }

  function vBar_chart1(_selection) {
    _selection.each(function (_data) {
      var chartW = width - margin.left - margin.right,
        chartH = height - margin.top - margin.bottom;

      var x1 = d3.scale.ordinal()
        .domain(_data.map(labelDisplay))
        .rangeRoundBands([0, chartW], .1);

      var y1 = d3.scale.linear()
        .domain([0, d3.max(_data, function (d, i) {
          return _data[i][1];
        })])
        .range([chartH, 0]);

      var xAxis = d3.svg.axis()
        .scale(x1)
        .orient('bottom');

      var yAxis = d3.svg.axis()
        .scale(y1)
        .orient('left');

      var barW = chartW / _data.length;

      if (!svg) {
        svg = d3.select(this)
          .append('svg')
          .classed('chart', true);
        var container = svg.append('g').classed('container-group', true);
        container.append('g').classed('chart-group', true);
        container.append('g').classed('x-axis-group axis', true);
        container.append('g').classed('y-axis-group axis', true);
      }

      svg.transition().duration(duration).attr({ width: width, height: height })
      svg.select('.container-group')
        .attr({ transform: 'translate(' + margin.left + ',' + margin.top + ')' });

      svg.select('.x-axis-group.axis')
        .transition()
        .duration(duration)
        .ease(ease)
        .attr({ transform: 'translate(0,' + (chartH) + ')' })
        .call(xAxis);

      svg.select('.y-axis-group.axis')
        .transition()
        .duration(duration)
        .ease(ease)
        .call(yAxis);

      var gapSize = x1.rangeBand() / 100 * gap;
      var barW = x1.rangeBand() - gapSize;

      _data.sort(dataComparitor);

      var bars = svg.select('.chart-group')
        .selectAll('.bar')
        .data(_data);
      bars.enter().append('rect')
        .classed('bar', true)
        .attr({ x: chartW,
          width: barW,
          y: function (d, i) {
            return y1(d[1]);
          },
          height: function (d, i) {
            return chartH - y1(d[1]);
          }
        })
        .on('mouseover', dispatch.customHover)
        .append("svg:title")
        .text(function (d) {
          return (d[0] + ':  ' + formatMetric('currency', d[1]));
        });
      bars.transition()
        .duration(duration)
        .ease(ease)
        .attr({
          width: barW,
          x: function (d, i) {
            return x1(i) + gapSize / 2;
          },
          y: function (d, i) {
            return y1(d[1]);
          },
          height: function (d, i) {
            return chartH - y1(d[1]);
          }
        });
      bars.exit().transition().style({ opacity: 0 }).remove();

      bars.on("click", function () {
        handleVBarChart1Click();
      })

      duration = 500;

      function dataComparitor(a, b) {
        return dataComparitorSortByName(a, b);
      }

      function dataComparitorSortByName(a, b) {
        if (currentSort === "desc") {
          return d3.descending(a[0], b[0]);
        }
        return d3.ascending(a[0], b[0]);
      }

      function dataComparitorSortByMetric(a, b) {
        if (currentSort === "desc") {
          return d3.descending(a[1], b[1]);
        }
        return d3.ascending(a[1], b[1]);
      }

      function labelDisplay(d, i) {

        var t = _data[i][0];
        return t;
      }

      function formatMetric(type, value) {
        if (type == 'currency') {
          var DecimalSeparator = Number("1.2").toLocaleString().substr(1, 1);

          var AmountWithCommas = value.toLocaleString();
          var arParts = String(AmountWithCommas).split(DecimalSeparator);
          var intPart = arParts[0];
          var decPart = (arParts.length > 1 ? arParts[1] : '');
          decPart = (decPart + '00').substr(0, 2);

          return '$' + intPart + DecimalSeparator + decPart;

        }
      }

      function redrawVBarChart1() {


        var x1_copy = d3.scale.ordinal()
          .domain(_data.map(labelDisplay))
          .rangeRoundBands([0, chartW], .1);

        var y1_copy = d3.scale.linear()
          .domain([0, d3.max(_data, function (d, i) {
            return _data[i][1];
          })])
          .range([chartH, 0]);

        xAxis = d3.svg.axis()
          .scale(x1_copy)
          .orient('bottom')
          .outerTickSize(100);


        // draw the x-axis
        // if there are more than 5 bars, rotate the text by 15 degrees
        if (_data.length <= 5) {
          svg.select('.x-axis-group.axis')
            .transition()
            .duration(duration)
            .ease(ease)
            .attr({ transform: 'translate(0,' + (chartH) + ')' })
            .call(xAxis);
        }
        else {
          svg.select('.x-axis-group.axis')
            .transition()
            .duration(duration)
            .ease(ease)
            .attr({ transform: 'translate(0,' + (chartH) + ')' })
            .call(xAxis)
            .selectAll("text")
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", function (d) {
              return "rotate(-15)"
            });

        }


        bars.data(_data)
          .transition()
          .delay(function (d, i) {
            return i * 50
          })
          .duration(500)
          .ease(ease)
          .attr({
            width: barW,
            x: function (d, i) {
              return x1_copy(i) + gapSize / 2;
            },
            y: function (d, i) {
              return y1_copy(d[1]);
            },
            height: function (d, i) {
              return chartH - y1_copy(d[1]);
            }
          })
          .selectAll("svg:title")
          .text(function (d) {
            return (d[0] + ':  ' + formatMetric('currency', d[1]));
          })
        ;

      };

      function handleVBarChart1Click() {
        if (currentSort === '')
          currentSort = "desc";
        else
          currentSort = '';
        _data.sort(dataComparitor);
        redrawVBarChart1();
      };


      redrawVBarChart1();
    });
  }


  exports.width = function (_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };
  exports.height = function (_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    duration = 0;
    return this;
  };
  exports.defaultDataViz = function (_x) {
    if (!arguments.length)  return defaultDataViz;
    defaultDataViz = _x;
    return this;
  }
  exports.gap = function (_x) {
    if (!arguments.length) return gap;
    gap = _x;
    return this;
  };
  exports.ease = function (_x) {
    if (!arguments.length) return ease;
    ease = _x;
    return this;
  };
  d3.rebind(exports, dispatch, 'on');
  return exports;
};