'use strict';

/* App Module */

var adminApp = angular.module('adminApp', ['nobiDirectives']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/users', {templateUrl: 'partials/user-list.html', controller: 'UsersCtrl'}).
            //when('/user/:userId', {templateUrl: 'partials/user-detail.html', controller: 'UserDetailCtrl'}).
            otherwise({redirectTo: '/users'});
    }]);
