from urllib2 import *
from django.http import HttpResponse
import urllib
import json
from QueryContext import QueryContext
from FacetParser import FacetParser
from FilterParser import FilterParser
from UserManager import UserManager
from django.conf import settings
from django.template import Context, loader

def _createResponse(request, data_object):
    callback_string = request.GET.get('callback')
    callback_response = callback_string + "(" + json.dumps(data_object, indent=4) + ")"
    return HttpResponse( callback_response, content_type='text/javascript')

def userinfo(request):
    callback_string = request.GET.get('callback')
    if request.session.get('member_id'):
        json_response = request.session.get('user_name')
    else:
        json_response = 'anonymous'

    callback_response = callback_string + "(" + json.dumps(json_response, indent=4) + ")"
    return HttpResponse( callback_response, content_type='text/javascript')

def login(request):
    callback_string = request.GET.get('callback')
    user_name = request.GET.get('username')
    password = request.GET.get('password')
    user_manager = UserManager()
    success = False

    try:
        user_id = user_manager.verify_user(user_name, password)
        request.session['user_name'] = user_name
        request.session['member_id'] = user_id
        message = "user validated"
        success = True
    except ValueError as e:
        message = str(e)

    callback_response = callback_string + "(" + json.dumps({'success': success, 'username': user_name, 'message': message}, indent=4) + ")"
    return HttpResponse( callback_response, content_type='text/javascript')

def adduser(request):
    user_name = request.GET.get('username')
    user_manager = UserManager()

    try:
        response = user_manager.add_user(user_name)
        message = "User created with temporary password: " + response
        success = True
    except ValueError as e:
        message = str(e)
        response = ""
        success = False

    data_object = {'success': success, 'temporary_password': response, 'message': message}
    return _createResponse(request, data_object)

def users(request):
    callback_string = request.GET.get('callback')
    user_manager = UserManager()
    users = user_manager.list_users()
    return _createResponse(request, users)

def autocomplete(request):
    querystring = request.GET.get('q').lower()
    callback_string = request.GET.get('callback')
    query_context = QueryContext(querystring)

    #set up base URL
    nobiURL = settings.SOLR_URL
    nobiURL += query_context.data_collection
    nobiURL += "/select?q=*%3A*&wt=json&indent=true&facet=true&rows=0"

    facet_prefix = urllib.quote_plus(querystring.strip())

    nobiURL += "&facet.prefix=" + facet_prefix

    query_fields = query_context.default_fact_facet.split(',')
    for query_field in query_fields:
        nobiURL += "&facet.field=" + query_field + "_auto"

    #execute solr call
    conn = urlopen(nobiURL)
    json_response = json.loads(conn.read())
    conn.close()

    #find all the autocomplete results
    autocomplete_results = []
    for query_field in query_fields:
        facet_results = json_response['facet_counts']['facet_fields'][query_field + '_auto']
        index = -1
        for facet_result in facet_results:
            index = index + 1
            if ((index % 2) == 0):
                autocomplete_results.append(facet_result)
        
    #clean out the parts we don't need    
    del json_response['response']
    del json_response['facet_counts']

    #sort results and inject them
    autocomplete_results.sort()
    json_response['autocomplete_results'] = autocomplete_results

    callback_response = callback_string + "(" + json.dumps(json_response, indent=4) + ")"
    return HttpResponse( callback_response, content_type='text/javascript')

def queryparser(request):
    querystring = request.GET.get('q')
    #facetstring = request.GET.get('f')
    filterlist = request.GET.getlist('fq')
    filterstring = request.GET.get('fq')
    callback_string = request.GET.get('callback')

    query_context = QueryContext(querystring)

    #set up base URL
    nobiURL = settings.SOLR_URL
    nobiURL += query_context.data_collection
    nobiURL += "/select?wt=json&indent=true"
    nobiURL += "&defType=" + query_context.type
    nobiURL += "&qf=" + query_context.fields
    nobiURL += "&stats=true&stats.field=" + query_context.stats_metric
    nobiURL += "&stats.facet=" + query_context.stats_facet
    nobiURL += "&q=" + urllib.quote_plus(query_context.parsed_query.strip())

    #add facets
    facet_parser = FacetParser(query_context.default_fact_facet)
    nobiURL += facet_parser.facet_solrstring

    #add filter
    filter_parser = FilterParser(filterlist)
    nobiURL += filter_parser.filter_solrstring

    #execute solr call
    conn = urlopen(nobiURL + "&rows=0")
    json_response = json.loads(conn.read())
    conn.close()

    del json_response['response']

    json_response['responseHeader']['nobi_reports'] = query_context.relevant_reports

    #inject default facets into the response
    json_response['responseHeader']['nobi_facets'] = query_context.default_fact_facet

    #inject default visualization into the response
    json_response['responseHeader']['nobi_dataviz'] = 'bar-chart'

    #format stats facets into 'dimension' node
    stats_json = json_response['stats']['stats_fields'][query_context.stats_metric]
    if stats_json is not None:
        stats_json['facets']['dimension1'] = stats_json['facets'][query_context.stats_facet]

    callback_response = callback_string + "(" + json.dumps(json_response, indent=4) + ")"
    return HttpResponse( callback_response, content_type='text/javascript')


def index(request):
    t = loader.get_template('home.html')
    c = Context({
    })
    return HttpResponse(t.render(c))
