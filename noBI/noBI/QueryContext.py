class QueryContext:
    def __init__(self, querystring):
        self.data_collection = "collection1"
        self.type = "edismax"
        self.fields = "description+channel_name+category_name"
        self.fields += "+company_name+account_name+department_name"
        self.fields += "+intercompany_name+brand_name"
        self.stats_facet = "description"
        self.stats_metric = "activity"
        self.default_fact_facet = "description,company,account"
        self.default_fact_facet += ",department,intercompany"
        self.default_fact_facet += ",channel,brand"
        self.default_fact_facet += ",category"
        self.parsed_query = ""
        self.relevant_reports = []

        self.parse_query(querystring)
        self.suggest_reports()


    def suggest_reports(self):
        qs = self.parsed_query.lower()

        print qs

        #this is a sample link
        report_url = "http://www.casertaconcepts.com/blog-2/its-a-big-nosql-world-out-there/"


        #report_name = "SAMPLE REPORT"
        #self.relevant_reports.append([report_name, report_url])

        if qs == "net sales ":
            report_name = "COO Report"
            self.relevant_reports.append([report_name, report_url])
        if qs == "cogs ":
            report_name = "Interesting Industry Report"
            self.relevant_reports.append([report_name, report_url])
        


    def parse_query(self, query):
        token_list = query.split()
        in_by_clause = False
        add_token = True
        
        for token in token_list:
            if in_by_clause:
                self.stats_facet = token
                in_by_clause = False
                add_token = False
            if token.lower() == "by":
                in_by_clause = True
                add_token = False
            if add_token:
                self.parsed_query += token + " "
            add_token = True
            

    def tokenize_string(input_string):
        tokens = input_string.split()
        return tokens
