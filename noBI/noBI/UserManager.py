import solr
import uuid
import urllib
from django.conf import settings
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

class UserManager(BaseUserManager):
    def __init__(self):
        super(UserManager, self).__init__()
        self.data_collection = "noBI_admin"
        self.noBI_url = settings.SOLR_URL + self.data_collection

    def list_users(self):
        users = []
        s = solr.SolrConnection(self.noBI_url)
        response = s.query('*:*')

        for hit in response.results:
            data = { "username" : hit['username'], "id" : hit['id'], "admin_datatype" : hit['admin_datatype'], "is_password_temporary" : hit["is_password_temporary"] }
            users.append(data)

        return users

    def get_user(self, user_name):
        user = User()
        s = solr.SolrConnection(self.noBI_url)
        fq_username = "username:" + user_name

        response = s.query('*:*', fq=[fq_username])
        intCount = len(response.results)

        user.id = ""
        for hit in response.results:
            user.id = hit['id']
            user.admin_datatype = hit['admin_datatype']
            user.password = hit['password']
            user.is_password_temporary = hit['is_password_temporary'] or True

        return user

    def verify_user(self, user_name, password):
        user = self.get_user(user_name)

        if user.id == "":
            raise ValueError("User " + user_name + " was not found.")

        if not user.check_password(password):
            raise ValueError("Password is invalid.")

        return user.id

    def add_user(self, user_name):
        if  not user_name:
            raise ValueError("Please specify a user name.")

        s = solr.SolrConnection(self.noBI_url)

        user = self.get_user(user_name)

        if user.id == "":
            user.username = user_name
            p = self.make_random_password(length=6, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
            user.set_password(p)

            s.add(id=uuid.uuid4(), username=user_name, admin_datatype="User", password=user.password, is_password_temporary=True)
            s.commit()

            return p

        else:
            raise ValueError("User " + user_name + " already exists.")

class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'