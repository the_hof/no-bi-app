import urllib

class FilterParser:
    def __init__(self, filter_querylist):
        self.filter_solrstring = ""
        if filter_querylist:
            for fq in filter_querylist:
                self.filter_solrstring += "&fq="
                self.filter_solrstring += urllib.quote_plus(fq)
