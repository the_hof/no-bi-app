from django.conf.urls import patterns, include, url
from django.conf.urls import *
from noBI.views import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'noBI.views.home', name='home'),
    # url(r'^noBI/', include('noBI.foo.urls')),
    (r'^$', index),
    (r'^q/$', queryparser),
    (r'^auto/$', autocomplete),
    (r'^noBIadmin/adduser/$', adduser),
    (r'^noBIadmin/login/$', login),
    (r'^noBIadmin/userinfo/$', userinfo),
    (r'^noBIadmin/users/$', users),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
