class FacetParser:
    def __init__(self, facet_querystring):
        if not facet_querystring:
            self.facet_solrstring = ""
        else:
            facet_params = facet_querystring.split(',')
            self.facet_solrstring = "&facet=true&facet.sort=index"
            for facet_param in facet_params:
                self.facet_solrstring += "&facet.field=" + facet_param
